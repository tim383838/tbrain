import pandas as pd
import numpy as np
from joblib import Parallel, delayed
import time

def preprocess_rolling_features(df):
    # 類別變數rolling
    groupby_list = ['bacno' , 'cano']
    rolling_len_list = ['6h' , '7d']
    category_features = 'stocn'
    df = Rolling_cat_variable_1(df , groupby_list ,category_features, rolling_len_list )
    category_features = 'mchno'
    df = Rolling_cat_variable_1(df , groupby_list ,category_features, rolling_len_list )
    category_features = 'mcc'
    df = Rolling_cat_variable_1(df , groupby_list ,category_features, rolling_len_list )

    # 數值變數rolling
    groupby_list = ['bacno' , 'cano']
    rolling_len_list = ['6h' , '7d']
    category_features = 'conam'
    df = Rolling_num_variable_2(df , groupby_list ,category_features, rolling_len_list )

    return df


def Rolling_cat_variable_1(df , groupby_list ,category_features, rolling_len_list ):
    df['absolute_time'] =df[['global_time']]+3600*16-29#常數是為了控制時間從０點開始
    #要換成time stamp的型態才可以rolling
    df['time_index'] =  df['absolute_time'].apply(lambda x :pd.Timestamp(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(x))))
    # assign index 後才能rolling by day
    df.index = df['time_index'].tolist()
    # 訓練前要先轉好排列順序
    df = df.sort_values(by= groupby_list + ['time_index'])
    for i in rolling_len_list:
        features_name = '_'.join(groupby_list)+'_'+category_features+'_unique_'+i
        #　min_periods=1，代表rolling過程中只要有一個有數值，其他即使是null也會補
        x = df.groupby( groupby_list  )[ category_features ].rolling(i,min_periods=1).apply(lambda x :len(np.unique(x)) )
        features =x.tolist()
        df[ features_name ] = features  
    df.reset_index(inplace= True)
    df.drop(['absolute_time','time_index'] , axis =1,inplace=True )
    df = df[   df.columns[ df.columns.tolist().index('acqic') : ].tolist()   ]
    return df 

def Rolling_num_variable_2(df , groupby_list ,category_features, rolling_len_list ):
    df['absolute_time'] =df[['global_time']]+3600*16-29#常數是為了控制時間從０點開始
    #要換成time stamp的型態才可以rolling
    df['time_index'] =  df['absolute_time'].apply(lambda x :pd.Timestamp(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(x))))
    # assign index 後才能rolling by day
    df.index = df['time_index'].tolist()
    # 訓練前要先轉好排列順序
    df = df.sort_values(by= groupby_list + ['time_index'])
    df['count'] =1
    for i in rolling_len_list:
        features_name_mean = '_'.join(groupby_list)+'_'+category_features+'_mean_'+i
        features_name_sum = '_'.join(groupby_list)+'_'+category_features+'_sum_'+i
        x = df.groupby( groupby_list )[category_features].rolling(i , min_periods=1).mean()
        y = df.groupby( groupby_list )['count'].rolling(i , min_periods=1).sum()
        features1 = x.tolist()
        features2 = y.tolist()

        df[ features_name_mean ] = features1    
        df[ features_name_sum ] = features2
    df.reset_index(inplace= True)
    df.drop(['absolute_time','time_index','count'] , axis =1,inplace=True )
    df = df[   df.columns[ df.columns.tolist().index('acqic') : ].tolist()   ]
    return df 