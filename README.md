# Tbrain

分類模型實例應用於金融場景

## Repo說明
透過客戶的消費行為資訊，去檢視每筆消費是否為盜刷，屬於0&1二分類模型預測專案，本專案參考Tbrain第三名團隊提供的程式，進行進一步修改，並於後續驗測成效達到前10名的成績。

## 資料說明
欄位 說明 <br>
bacno 歸戶帳號 <br>
txkey 交易序號 <br>
locdt 授權日期 <br>
loctm 授權時間 <br>
cano 交易卡號 <br>
contp 交易類別 <br>
etymd 交易型態 <br>
mchno 特店代號 <br>
acqic 收單行代碼 <br>
mcc MCC_CODE <br>
conam 交易金額-台幣 <br>
ecfg 網路交易註記 <br>
insfg 分期交易註記 <br>
iterm 分期期數 <br>
stocn 消費地國別 <br>
scity 消費城市 <br>
stscd 狀態碼 <br>
ovrlt 超額註記碼 <br>
flbmk Fallback 註記 <br>
hcefg 支付形態 <br>
csmcu 消費地幣別 <br>
flg_3dsmk 3DS 交易註記 <br>
fraud_ind 盜刷註記 <br>
## 參考資料
https://github.com/aarontong95/TBrain_Credit_Card

